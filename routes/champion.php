<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Champion\ChampionController;

Route::get('/champion/create', [ChampionController::class, 'create'])->name('champion.create')->middleware('auth', 'admin');

Route::middleware('auth', 'admin')->group(function () {

    Route::get('/champion', [ChampionController::class, 'index'])->name('champion');


    Route::post('/champion', [ChampionController::class, 'store'])->name('champion.store');

    Route::get('/champion/{champion}/edit', [ChampionController::class, 'edit'])->name('champion.edit');
    Route::post('/champion/update', [ChampionController::class, 'update'])->name('champion.update');

    Route::get('/champion/del/{delete}', [ChampionController::class, 'delete'])->name('champion.delete');




});

Route::get('champion/{post}', [ChampionController::class, 'show'])->name('champion.show');
?>
