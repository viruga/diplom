<?php

use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;



//Route::prefix('user')->middleware(['auth', 'active'])->group(function () {
Route::prefix('admin')->middleware('auth', 'admin')->group(function () {
    Route::view('/', [AdminController::class, 'index'])->name('admin')->withoutMiddleware('admin');
    Route::get('/', [AdminController::class, 'index'])->name('admin')->withoutMiddleware('admin');
    Route::get('posts', [AdminController::class, 'posts'])->name('admin.posts');
    Route::get('posts/create', [AdminController::class, 'create'])->name('admin.posts.create');
    Route::post('posts/create', [AdminController::class, 'store'])->name('admin.posts.store');
    Route::get('posts/{post}/edit', [AdminController::class, 'edit'])->name('admin.posts.edit');
    Route::get('posts/{post}', [AdminController::class, 'delete'])->name('admin.posts.delete');
    Route::post('posts', [AdminController::class, 'update'])->name('admin.posts.update');

//    Route::view('/person', 'person')->name('user.person');
//    Route::get('posts', [UserController::class, 'index'])->name('user.posts');
//    Route::get('posts/create', [UserController::class, 'create'])->name('user.posts.create');
//    Route::post('posts', [UserController::class, 'store'])->name('user.posts.store');
//    Route::get('posts/{post}', [UserController::class, 'show'])->name('user.posts.show');
//    Route::put('posts/{post}/edit', [UserController::class, 'edit'])->name('user.posts.edit');
//    Route::put('posts/{post}', [UserController::class, 'update'])->name('user.posts.update');
//    Route::delete('posts/{post}', [UserController::class, 'delete'])->name('user.posts.delete');
});

Route::prefix('person')->middleware('auth')->group(function () {

    Route::view('/', [UserController::class, 'index'])->name('person');
    Route::get('/', [UserController::class, 'index'])->name('person');
    Route::get('/avatar', [UserController::class, 'avatar'])->name('person.avatar');
    Route::post('/avatar', [UserController::class, 'upload'])->name('person.avatar.upload');
//    Route::view('/person', 'person')->name('user.person');
//    Route::get('posts', [UserController::class, 'index'])->name('user.posts');
//    Route::get('posts/create', [UserController::class, 'create'])->name('user.posts.create');
//    Route::post('posts', [UserController::class, 'store'])->name('user.posts.store');
//    Route::get('posts/{post}', [UserController::class, 'show'])->name('user.posts.show');
//    Route::put('posts/{post}/edit', [UserController::class, 'edit'])->name('user.posts.edit');
//    Route::put('posts/{post}', [UserController::class, 'update'])->name('user.posts.update');
//    Route::delete('posts/{post}', [UserController::class, 'delete'])->name('user.posts.delete');
});
?>
