<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Champion\ChampionController;
use App\Http\Controllers\Car\CarController;
use App\Http\Controllers\Car\HaracteristicController;


Route::middleware('auth', 'admin')->group(function () {

    Route::get('/car/show/{id}', [CarController::class, 'show'])->name('car.show');
    Route::get('/car/create', [CarController::class, 'create'])->name('car.create');
    Route::post('/car', [CarController::class, 'store'])->name('car.store');
    Route::get('/car/{car}', [CarController::class, 'delete'])->name('car.delete');


});


Route::middleware('auth', 'admin')->group(function () {

    Route::get('/haracteristic/create', [HaracteristicController::class, 'create'])->name('haracteristic.create');
    Route::post('/haracteristic', [HaracteristicController::class, 'store'])->name('haracteristic.store');
    Route::get('/haracteristic/{id}', [HaracteristicController::class, 'delete'])->name('haracteristic.delete');

});



?>
