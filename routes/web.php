<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Register\RegisterController;
use App\Http\Controllers\Login\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\Champion\ChampionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'show'])->name('home');
Route::get('/home', [PostController::class, 'show'])->name('home');

Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/slider/create', [SliderController::class, 'create'])->name('slider.create');
Route::post('/slider/create', [SliderController::class, 'store'])->name('slider.store');
Route::get('/slider/{slide}', [SliderController::class, 'delete'])->name('slider.delete');

Route::middleware('guest')->group(function() {
    Route::get('register', [RegisterController::class, 'index'])->name('register');
    Route::post('register', [RegisterController::class, 'store'])->name('register.store');

    Route::get('login', [LoginController::class, 'index'])->name('login');
    Route::post('login', [LoginController::class, 'store'])->name('login.store');


});
