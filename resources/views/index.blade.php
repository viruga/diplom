@extends('layouts.base')
@section('content')
    @include('includes.slider')
    @include('includes.newsline')
    @include('includes.champions')
@endsection
