@extends('layouts.user')
@section('content')
        @include('includes.user.admin.menu')
        @section('page.status', 'Админка')
        <div class="editor">
            <h2>Редактирование профиля</h2>
            <div class="editor__info">
                <p>Имя пользователя: {{ Auth::user()->name }}</p>
                <p>Дата регистрации: {{ Auth::user()->created_at }}</p>
                <p>Статус:
                    @if ((Auth::user()->admin===1))
                        Администратор
                        <h2>Последние новости:</h2>
                        @if(empty($posts))
                            {{ __('В данный момент постов нет.') }}
                        @else
                            <div class="posts">
                            @foreach($posts as $post)
                                <div class="post__single">
                                    <span class="post__edit"><a href="{{ route('admin.posts.edit', $post->id) }}">Изменить</a></span>
                                    <div class="post__title">{{ $post->title }}</div>
                                    <span class="post__edit"><a href="{{ route('admin.posts.delete', $post->id) }}">Удалить</a></span>
                                </div>
                            @endforeach
                            </div>
                        @endif

                        <h2>Последние чемпионы:</h2>
                        @if(empty($posts))
                            {{ __('В данный момент чемпионов нет.') }}
                        @else
                            <div class="posts">
                                @foreach($champions as $champion)
                                    <div class="post__single">
                                        <span class="post__edit"><a href="{{ route('champion.edit', $champion->id) }}">Изменить</a></span>
                                        <div class="post__title">{{ $champion->name }}</div>
                                        <span class="post__edit"><a href="{{ route('champion.delete', $champion->id) }}">Удалить</a></span>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        <h2>Последние слайды:</h2>
                        @if(empty($posts))
                            {{ __('В данный момент слайдов нет.') }}
                        @else
                            <div class="posts">
                                @foreach($sliders as $slider)
                                    <div class="post__single slider_btn">
                                        <div class="post__title slider">{{ $slider->name }}</div>
                                        <span class="post__edit slider_btn"><a href="{{ route('slider.delete', $slider->id) }}">Удалить</a></span>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @else
                        Пользователь
                    @endif
                </p>
            </div>
        </div>
@endsection
