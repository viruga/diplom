@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Добавление картинки в слайдер:</h2>
        <div class="create__post">
            @if($errors->any())
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $message)
                            <li>
                                - {{ $message }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('slider.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <label for="image">Изображение статьи:</label>
                    <input type="text" name="image" autofocus/>
                    <input type="file" name="image">

                    <p>
                        <label for="title">Заголовок:</label>
                        <input name="title" id="title" type="text" value="{{ old('title') }}">
                    </p>
                    <span>Минимальный размер изображения 1280px * 440px</span>
                    <p>
                        <button type="submit" class="post__button" name="submit">Создать</button>
                        <button type="reset" class="post__button" name="reset">Очистить</button>
                    </p>
                </fieldset>
            </form>
        </div>

    </div>
@endsection
