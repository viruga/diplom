@if(empty($sliders))
    {{ __('В данный момент слайдов нет.') }}
@else
<div class="itcss">
    <div class="itcss__wrapper">
        <div class="itcss__items">
            @foreach($sliders as $slider)
                <div class="itcss__item">
                    <img src="{{ Storage::url($slider->image) }}">
                </div>
            @endforeach
        </div>
    </div>
    <!-- Стрелки для перехода к предыдущему и следующему слайду -->
    <a class="itcss__control itcss__control_prev" href="#" role="button" data-slide="prev"></a>
    <a class="itcss__control itcss__control_next" href="#" role="button" data-slide="next"></a>
    @endif
</div>

