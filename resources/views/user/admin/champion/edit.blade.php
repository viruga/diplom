@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Редактирование информации:</h2>
        <div class="create__post">
            @if($errors->any())
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $message)
                            <li>
                                - {{ $message }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form action="{{route('champion.update')}}" method="post" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <div class="photo">
                        <label for="image">Фото:</label>
                        @if (($champion->image!=0))

                            <p><img src="{{ Storage::url($champion->image) }}"></p>
                            <input type="text" name="image" value="" autofocus/>
                            <input type="file" name="image" value="{{ Storage::url($champion->image) }}">
                        @else

                            <p><input type="text" name="image" autofocus/></p>
                            <input type="file" name="image">
                         @endif
                    </div>

                    <p>
                        <input name="id" id="id" type="hidden" value="{{ $champion->id }}">
                        <label for="name">Имя:</label>
                        <input name="name" id="name" type="text" value="{{ $champion->name }}">
                    </p>
                    <p>
                        <input id="content" type="hidden" name="content" value="{!! $champion->content !!}">
                        <trix-editor input="content"></trix-editor>
                    </p>
                    <p>
                        <button type="submit" class="post__button" name="submit">Изменить</button>
                        <button type="reset" class="post__button" name="reset">Очистить</button>
                    </p>
                </fieldset>
            </form>
        </div>

    </div>
@endsection
