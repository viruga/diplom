@extends('layouts.user')
@section('content')
        @include('includes.user.admin.menu')
        @section('page.status', 'Админка')
        <div class="editor">
            <h2>Редактирование профиля</h2>
            <div class="editor__info">
                <p>Имя пользователя: {{ Auth::user()->name }}</p>
                <p>Дата регистрации: {{ Auth::user()->created_at }}</p>
                <p>Статус:
                    @if ((Auth::user()->admin===1))
                        Администратор
                        <h2>Последние чемпионы:</h2>
                        @if(empty($champions))
                            {{ __('В данный момент чемпионов нет.') }}
                        @else
                            <div class="posts">
                                @foreach($champions as $champion)
                                    <div class="post__single">
                                        <span class="post__edit"><a href="{{ route('champion.edit', $champion->id) }}">Изменить</a></span>
                                        <div class="post__title">{{ $champion->name }}</div>
                                        <span class="post__edit"><a href="{{ route('champion.delete', $champion->id) }}">Удалить</a></span>
                                    </div>
                                    @if(isset($cars))
                                    @foreach($cars as $car)
                                       @if ($car->champion_id === $champion->id)
                                       <div class="post__single slider_btn">
                                           <div class="post__title slider"><a href="{{route('car.show', $car->id)}}">{{ $car->brend }} {{ $car->model }}</a></div>
                                           <span class="post__edit slider_btn"><a href="{{ route('car.delete', $car->id) }}">Удалить</a></span>
                                       </div>
                                       @endif
                                    @endforeach
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    @else
                        Пользователь
                    @endif
            </div>
        </div>
@endsection
