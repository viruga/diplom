@extends('layouts.base')

@section('content')
    <div class="fullnews">
        <div class="container">
            <h1 class="title">{{ $champion->title }}</h1>
            <div class="description">
                <div>
                    @if (($champion->image!=0))
                        <img src="{{ Storage::url($champion->image) }}">
                    @else
                        <img src="/img/no_photo.jpg">
                @endif
                </div>
                <div class="trix-content">{!! $champion->content !!}</div>

            </div>
            <span>Автомобиль:</span>
            @if (isset($cars))
                @foreach($cars as $car)
                    @if ($champion->id === $car->champion_id)
                    <div>{{ $car->brend }} {{ $car->model }}</div>
                        <p>Характеристики:</p>
                        @if (isset($haracteristics))
                            @foreach($haracteristics as $haracteristic)
                                @if ($car->id === $haracteristic->car_id)
                                    <div> - {{ $haracteristic->name }} {{ $haracteristic->text }}</div>
                                @endif
                            @endforeach
                        @endif
                    @endif
                @endforeach
            @endif
            @include('includes.champions')
    </div>
@endsection
