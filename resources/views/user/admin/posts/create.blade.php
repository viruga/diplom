@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Добавление новости:</h2>
        <div class="create__post">
            @if($errors->any())
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $message)
                            <li>
                                - {{ $message }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('admin.posts.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <label for="image">Изображение статьи:</label>
                    <input type="text" name="image" autofocus/>
                    <input type="file" name="image">
                    <p>
                        <label for="title">Заголовок:</label>
                        <input name="title" id="title" type="text" value="{{ old('title') }}">
                    </p>
                    <p>
                        <label for="description">Краткое описание:</label>
                    </p>
                    <p>
                        <textarea name="description" id="description">{{ old('description') }}</textarea>

                    </p>
                    <p>
                        <input id="content" type="hidden" name="content">
                        <trix-editor input="content"></trix-editor>
                    </p>
                    <p>
                        <button type="submit" class="post__button" name="submit">Создать</button>
                        <button type="reset" class="post__button" name="reset">Очистить</button>
                    </p>
                </fieldset>
            </form>
        </div>

    </div>
@endsection
