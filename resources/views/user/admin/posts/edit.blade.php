@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Добавление новости:</h2>
        <div class="create__post">
            @if($errors->any())
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $message)
                            <li>
                                - {{ $message }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form action="{{route('admin.posts.update')}}" method="post" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <div class="photo">
                        <label for="image">Изображение статьи:</label>
                        @if (($post->image!=0))

                            <p><img src="{{ Storage::url($post->image) }}"></p>
                            <input type="text" name="image" value="" autofocus/>
                            <input type="file" name="image" value="{{ Storage::url($post->image) }}">
                        @else

                            <p><input type="text" name="image" autofocus/></p>
                            <img src="/img/no_photo.jpg">
                            <input type="file" name="image">
                         @endif
                    </div>

                    <p>
                        <input name="id" id="id" type="hidden" value="{{ $post->id }}">
                        <label for="title">Заголовок:</label>
                        <input name="title" id="title" type="text" value="{{ $post->title }}">
                    </p>
                    <p>
                        <label for="description">Краткое описание:</label>
                    </p>
                    <p>
                        <textarea name="description" id="description">{{ $post->description }}</textarea>

                    </p>
                    <p>
                        <input id="content" type="hidden" name="content" value="{!! $post->content !!}">
                        <trix-editor input="content"></trix-editor>
                    </p>
                    <p>
                        <button type="submit" class="post__button" name="submit">Изменить</button>
                        <button type="reset" class="post__button" name="reset">Очистить</button>
                    </p>
                </fieldset>
            </form>
        </div>

    </div>
@endsection
