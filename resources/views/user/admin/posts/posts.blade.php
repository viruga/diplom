@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Редактирование профиля</h2>
        <div class="editor__info">
            <p>Имя пользователя: {{ Auth::user()->name }}</p>
            <p>Дата регистрации: {{ Auth::user()->created_at }}</p>
            <p>Статус: Пользователь</p>
        </div>
        <h2>Изменение аватарки:</h2>
        <div class="avatar__form">
            <form action="#" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="image">
                <button type="submit">Загрузить</button>
            </form>
        </div>

    </div>
@endsection
