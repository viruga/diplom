@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Добавление Характеристик:</h2>
        <div class="create__post">
            @if($errors->any())
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $message)
                            <li>
                                - {{ $message }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('haracteristic.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <label for="car">Автомобиль:</label>
                    <select name="car" id="car">
                        @if(empty($cars))
                            {{ __('Добавьте сначала автомобиль.') }}
                        @else
                                @foreach($cars as $car)
                                <option value="{{$car->id}}">{{$car->brend}} {{$car->model}}</option>
                                @endforeach
                        @endif
                    </select>
                    <p>
                        <label for="name">Характеристика:</label>
                        <input name="name" id="name" type="text" value="{{ old('name') }}">
                    </p>
                    <p>
                        <label for="text">Значение:</label>
                        <input name="text" id="text" type="text" value="{{ old('text') }}">
                    </p>
                    <p>
                        <button type="submit" class="post__button" name="submit">Создать</button>
                        <button type="reset" class="post__button" name="reset">Очистить</button>
                    </p>
                </fieldset>
            </form>
        </div>

    </div>
@endsection
