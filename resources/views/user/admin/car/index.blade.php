@extends('layouts.user')
@section('content')
        @include('includes.user.admin.menu')
        @section('page.status', 'Админка')
        <div class="editor">
            <h2>Редактирование профиля</h2>
            <div class="editor__info">
                <p>Имя пользователя: {{ Auth::user()->name }}</p>
                <p>Дата регистрации: {{ Auth::user()->created_at }}</p>
                <p>Статус:
                    @if ((Auth::user()->admin===1))
                        Администратор
                        <h2>Карточка автомобиля:</h2>
                            <div class="posts">
                                <div class="post__single slider_btn">
                                    <div class="post__title slider">{{ $car->brend }} {{ $car->model }}</div>
                                    <span class="post__edit slider_btn"><a href="{{ route('car.delete', $car->id) }}">Удалить</a></span>
                                </div>
                                @if(isset($haracteristics))
                                    @foreach($haracteristics as $haracteristic)
                                        <div class="post__single slider_btn">
                                           <div class="post__title slider">{{ $haracteristic->name }} {{ $haracteristic->text }}</div>
                                           <span class="post__edit slider_btn"><a href="{{ route('haracteristic.delete', $haracteristic->id) }}">Удалить</a></span>
                                       </div>
                                      @endforeach
                                @endif
                            </div>
                    @else
                        Пользователь
                    @endif
            </div>
        </div>
@endsection
