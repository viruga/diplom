@extends('layouts.user')
@section('content')
    @include('includes.user.admin.menu')
    <div class="editor">
        <h2>Добавление чемпиона:</h2>
        <div class="create__post">
            @if($errors->any())
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $message)
                            <li>
                                - {{ $message }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('car.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <label for="champion">Чей автомобиль:</label>
                    <select name="champion" id="champion">
                        @if(empty($champions))
                            {{ __('Добавьте сначала чемпиона.') }}
                        @else
                                @foreach($champions as $champion)
                                <option value="{{$champion->id}}">{{$champion->name}}</option>
                                @endforeach
                        @endif
                    </select>
                    <p>
                        <label for="brend">Бренд автомобиля:</label>
                        <input name="brend" id="brend" type="text" value="{{ old('brend') }}">
                    </p>
                    <p>
                        <label for="model">Модель автомобиля:</label>
                        <input name="model" id="model" type="text" value="{{ old('model') }}">
                    </p>
                    <p>
                        <button type="submit" class="post__button" name="submit">Создать</button>
                        <button type="reset" class="post__button" name="reset">Очистить</button>
                    </p>
                </fieldset>
            </form>
        </div>

    </div>
@endsection
