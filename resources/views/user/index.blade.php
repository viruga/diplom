@extends('layouts.user')
@section('content')
{{--    @section('page.username', 'viruga')--}}
{{--    @section('page.status', 'Админка')--}}
    <div class="editor">
        <h2>Редактирование профиля</h2>
        <div class="editor__info">
            <p>Имя пользователя: viruga</p>
            <p>Дата регистрации: 08.12.2023</p>
            <p>Статус: Пользователь</p>
            <form action="#" method="post">
                @csrf
                <input type="text" placeholder="{{ __('название статьи') }}" autofocus/>
                <input id="content" type="hidden" name="content">
                <trix-editor input="content"></trix-editor>
                <p><input type="submit" value="Отправить"></p>
            </form>
        </div>
    </div>
@endsection
