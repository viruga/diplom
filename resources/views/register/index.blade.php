@extends('layouts.base')
@section('page.style', '/style/login.css')
@section('page.title', 'Панель регистрации пользователя.')
@section('content')
    <div class="login-page" >
        <div class="form">
            @if($errors->any())
                <div class="errors">
                        <ul>
                            @foreach($errors->all() as $message)
                                <li>
                                    - {{ $message }}
                                </li>
                            @endforeach
                        </ul>
                </div>
            @endif

            <form class="login-form" action="#" method="post">
                    @csrf
                    <input type="text" name="name" placeholder="{{ __('имя пользователя') }}"/>
                    <input type="password" name="password" placeholder="{{ __('пароль') }} "/>
                    <input type="password" name="password_confirmation" placeholder="{{ __('подтверждения пароля') }} "/>
                    <input type="text" name="email" placeholder="{{ __('e-mail') }} "/>
                    <button>{{ __('Создать') }} </button>
                    <p class="message">{{ __('Вы зарегистрированы?') }} <a href="{{ route('login') }}">{{ __('Войти') }} </a></p>
                    <p class="message"><a href="{{ route('home') }}">{{ __('На главную') }}</a></p>
            </form>
        </div>
    </div>

@endsection
