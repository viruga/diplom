@if ($paginator->hasPages())
    <nav class="pagination">
        <div>
           <div class="page">
                <span>
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span class="hide__page">Назад</span>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}">Назад</a>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span>
                                <span>{{ $element }}</span>
                            </span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <span class="hide__page">
                                        <span>{{ $page }}</span>
                                    </span>
                                @else
                                    <a href="{{ $url }}">
                                        {{ $page }}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <a href="{{ $paginator->nextPageUrl() }}">Вперед</a>
                    @else
                        <span>
                            <span class="hide__page">
                                Вперед
                            </span>
                        </span>
                    @endif
                </span>
            </div>
        </div>
        <div class="hide">
            <p>
                {!! __('(Показаны с') !!}
                @if ($paginator->firstItem())
                    <span>{{ $paginator->firstItem() }}</span>
                    {!! __('по') !!}
                    <span>{{ $paginator->lastItem() }}</span>
                @else
                    {{ $paginator->count() }}
                @endif
                {!! __('новости из') !!}
                <span>{{ $paginator->total() }}</span>
                {!! __(')') !!}
            </p>
        </div>
    </nav>
@endif
