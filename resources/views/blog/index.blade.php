@extends('layouts.base')
@section('page.title', 'Новости дрифта. Мероприятия, чемпионаты.')
@section('page.description', 'Дрифт новости, вся самая интересная и новая информация о дрифте.')
@section('page.keywords', 'Новости, дрифт, мероприятия, результаты, pro, pro2.')
@section('content')
    <div class="news">
        <div class="container">
    <span class="title__section">{{ __('Новости') }}</span>
    <div class="news__list">
        @if(empty($posts))
            {{ __('В данный момент постов нет.') }}
        @else
            <div class="news__list">
                @foreach($posts as $post)
                    <div class="news__onсe">
                        @if (($post->image!=0))
                            <img src="{{ Storage::url($post->image) }}">
                        @else
                            <img src="/img/no_photo.jpg">
                        @endif
                        <span class="post__title">{{ $post->title }}</span>
                        <span class="post__description">{{ $post->description }}</span>
                        <span class="time">{{ $post->created_at->diffForHumans() }} <a href="{{ route('blog.show', $post->id) }}">Читать полностью...</a></span>
                    </div>
                @endforeach
            </div>
            {{ $posts->links() }}
        @endif
    </div>
        </div>
    </div>
@endsection
