@extends('layouts.base')

@section('content')
    <div class="fullnews">
        <div class="container">
            <h1 class="title">{{ $post->title }}</h1>
            <div class="description">
                <div>
                    @if (($post->image!=0))
                        <img src="{{ Storage::url($post->image) }}">
                    @else
                        <a href="{{ route('blog') }}"></a><img src="/img/no_photo.jpg">
                @endif
                </div>
                <div class="trix-content">{!! $post->content !!}</div>

            </div>
        <div class="more__news">
        <span>Читать еще:</span>
        @if(empty($posts->toArray()))
        @else
            @foreach($posts as $post)
                <p><a href="{{ route('blog.show', $post->id) }}">{{ $post->title }}</p>
            @endforeach
        @endif</div>
        </div>
    </div>
@endsection
