<div class="navigation">
    <div class="avatar">
        @if ((Auth::user()->avatar!=0))
            <img src="{{ Storage::url(Auth::user()->avatar) }}">

        @else
            <img src="/img/user_logo/no_photo_champion.png">
        @endif
    </div>
    @if ((Auth::user()->admin===1))
        <div class="block__navigation">
            <div class="block__title">Посты</div>
            <div class="block__content">
                <a href="{{ route ('admin') }}">Список</a>
                <a href="{{ route ('admin.posts.create') }}">Добавление статьи</a>
                <a href="{{ route ('slider.create') }}">Слайдер</a>
            </div>
        </div>
        <div class="block__navigation">
            <div class="block__title">Чемпионы</div>
            <div class="block__content">
                <a href="{{ route ('champion') }}">Список</a>
                <a href="{{ route ('champion.create') }}">Добавить чемпиона</a>
                <a href="{{ route ('car.create') }}">Добавить автомобиль</a>
                <a href="{{ route ('haracteristic.create') }}">Добавить характеристики</a>
            </div>
        </div>
    @endif
    <div class="block__navigation">
        <div class="block__title">Профиль</div>
        <div class="block__content">
            <a href="{{ route ('person.avatar') }}">Сменить аватар</a>
            <form action="{{ route ('logout') }}" method="post" >
                @csrf
                <button type="submit">{{ __('Выйти') }} </button>
            </form>
        </div>
    </div>

</div>
