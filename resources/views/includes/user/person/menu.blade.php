<div class="navigation">
    <div class="avatar">
        @if ((Auth::user()->avatar!=0))
            <img src="{{ Storage::url(Auth::user()->avatar) }}">

        @else
            <img src="/img/user_logo/no_photo_champion.png">
        @endif
    </div>
    <div class="block__navigation">
        <div class="block__title">Профиль</div>
        <div class="block__content">
            <a href="{{ route ('person.avatar') }}">Сменить аватар</a>
            <a href="">Изменить пароль</a>
            <form action="{{ route ('logout') }}" method="post" >
                @csrf
                <button type="submit">{{ __('Выйти') }} </button>
            </form>
        </div>
    </div>

</div>
