<div class="news">
    <div class="container">
        <span class="title__section"><a href="{{ route('blog') }}">{{ __('Новости') }}</a></span>
            @if(empty($posts))
                {{ __('В данный момент постов нет.') }}
            @else
                <div class="news__list">
                    @foreach($posts as $post)
                        <div class="news__onсe">
                            @if (($post->image!=0))
                                <img src="{{ Storage::url($post->image) }}">
                            @else
                                <img src="/img/no_photo.jpg">
                            @endif
                            <span class="post__title">{{ $post->title }}</span>
                            <p class="post__description">{{ $post->description }}</p>
                            <p><a href="{{ route('blog.show', $post->id) }}">Читать полностью...</a></p>
                            <span class="time">{{ $post->created_at->diffForHumans() }}</span>
                        </div>
                    @endforeach
                </div>
                <a href="{{ route('blog') }}">Все новости</a>
            @endif
    </div>
</div>
