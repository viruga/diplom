<div class="slider">
    <div class="itcss">
        <div class="itcss__wrapper">
            <div class="itcss__items">

                @if(empty($sliders->toArray()))
                    <div class="itcss__item">
                        <img src="../img/slider-1.jpg">
                    </div>
                    <div class="itcss__item">
                        <img src="../img/slider-2.jpg">
                    </div>
                @else
                    @foreach($sliders as $slider)
                        <div class="itcss__item">
                            <img src="{{ Storage::url($slider->image) }}">
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>








