<header class="header">
    <div class="container">
        <div class="header__menu">
            <img src="{{ asset('img/logo.jpg') }}" class="header__logo" alt="Logo">
            <nav class="header__nav">
                <a href="{{ route('home') }}">{{ __('Главная') }}</a> |
                <a href="{{ route('blog') }}">{{ __('Новости') }}</a> |
                @auth
                    @if ((Auth::user()->admin)!==1)
                        <a href="{{ route('person') }}">{{ __('Раздел пользователя') }}</a>
                    @else
                        <a href="{{ route('admin') }}">{{ __('Админка') }}</a>
                    @endif
                @endauth
                @guest
                    <a href="{{ route('register') }}">{{ __('Регистрация') }}</a> |
                    <a href="{{ route('login') }}">{{ __('Войти') }}</a>
                @endguest

            </nav>
        </div>
    </div>
</header>
