<div class="chempions">
    <div class="container">
        <span class="title__section">{{ __('Чемпионы') }}</span>
        <div class="chempions__list">
            @if(empty($champions->toArray()))
            @else
                @foreach($champions as $champion)
                    <div>
                        <span class="circle-image">
                        @if (($champion->image!=0))
                                <img src="{{ Storage::url($champion->image) }}">
                            @else
                                <img src="/img/user_logo/no_photo_champion.png">
                            @endif
                    </span>
                        <span class="name__champion"><a href="{{ route('champion.show', $champion->id) }}">{{ $champion->name }}</a></span>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
