<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('page.title', config('app.name'))</title>
    <meta name="description" content="@yield('page.description', 'Дипломный проект, блог для новосного блога по Дрифту. И еще чуть текста для того чтобы допить все 160 символов... Для поисковиков...')">
    <meta name="keywords" content="@yield('page.keywords', 'Диплом, блог, дрифт, новости, серия pro, rds.')">
    <link rel="stylesheet" type="text/css" href="/style/trix.css">
    <script type="text/javascript" src="/js/umd.min.js"></script>
    <link rel="stylesheet" href="/style/reset.css">
    <link rel="stylesheet" href="@yield('page.style', '/style/style.css')">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:wght@200;400;500;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/style/simple-adaptive-slider.css">
    <script type="text/javascript" src="/js/simple-adaptive-slider.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            // инициализация слайдера
            new ItcSimpleSlider('.itcss', {
                loop: true,
                autoplay: true,
                interval: 2000,
                swipe: true,
            });
        });

    </script>
</head>
<body>
@if(Route::currentRouteName()!=='login' and Route::currentRouteName()!=='register')
    @include('includes.header')
@endif
@yield('content')
@if(Route::currentRouteName()!=='login' and Route::currentRouteName()!=='register')
    @include('includes.footer')
@endif
</body>
</html>
