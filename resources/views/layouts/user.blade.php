<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Профиль пользователя</title>
    <meta name="description" content="Добавление контента, редактирование данных пользователя. Скрытый раздел сайта.">
    <link rel="stylesheet" href="/style/reset.css">
    <link rel="stylesheet" type="text/css" href="/style/trix.css">
    <script type="text/javascript" src="/js/umd.min.js"></script>
    <link rel="stylesheet" href="/style/user.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:wght@200;400;500;800&display=swap" rel="stylesheet">
</head>
<body>
<header class="header">
    <div class="container">
        <h1>@yield('page.status', 'Кабинет пользователя')</h1>
        <nav class="header__nav">
            <a href="{{ route ('home') }}">{{ __('Главная') }}</a> |
            <span>Пользователь: {{ Auth::user()->name }}</span>
        </nav>
    </div>
</header>
<div class="content">
    <div class="container">
        @yield('content')
    </div>
</div>
</body>
</html>
