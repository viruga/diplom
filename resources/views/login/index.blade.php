@extends('layouts.base')
@section('page.style', '/style/login.css')
@section('page.title', 'Панель входа.')
@section('content')
        <div class="login-page">
            <div class="form">
                @if($errors->any())
                    <div class="errors">
                        <ul>
                            @foreach($errors->all() as $message)
                                <li>
                                   - {{ $message }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="login-form" action="#" method="post">
                    @csrf
                    <input type="text" name="name" placeholder="{{ __('имя пользователя') }}" autofocus/>
                    <input type="password" name="password" placeholder="{{ __('пароль') }}"/>
                    <button>{{ __('вход') }}</button>
                    <p class="message">{{ __('Не зарегистрированы?') }} <a href="{{ route('register') }}">{{ __('Создать аккаунт') }}</a></p>
                    <p class="message"><a href="{{ route('home') }}">{{ __('На главную') }}</a></p>
                </form>
            </div>
        </div>


@endsection
