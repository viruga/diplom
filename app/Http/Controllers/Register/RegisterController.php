<?php

namespace App\Http\Controllers\Register;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index() {
        if(Auth::check()) {
            return redirect(route('user'));
        }
        return view('register.index');
    }

    public function store(Request $request) {
        $validated = $request->validate([
            //надо ли alpha_num?
            'name' => ['required', 'string', 'max:50', 'unique:users'],
            'email' => ['required', 'string', 'max:100', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'max:50', 'confirmed'],
        ]);

        User::query()->create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['password']),
        ]);

        return redirect()->route('login');

    }
}
