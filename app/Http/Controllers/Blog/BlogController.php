<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;


class BlogController extends Controller
{
    public function index(Request $request)
    {
        $validated = $request->validate([
            'page'=>['nullable', 'integer', 'min:1', 'max:100'],
        ]);

        $page = ($validated['page'] ?? 0) * 6 ;
        $posts = Post::query()->orderByDesc('created_at')->paginate(6,['id', 'title', 'image', 'description', 'created_at']);
        return view('blog.index', compact('posts' ));
    }

    public function show(Request $request, $post)
    {
         $post = Post::query()->findOrFail($post);
         $user = User::query()->findOrFail($post['user_id']);

            $postsCol = Post::all()->count();
         if ($postsCol<5) {
            $number = $postsCol;
         }
         else { $number = 5;}

         $posts = Post::all()->random($number);

        return view('blog.show', compact('post' , 'user', 'posts'));
    }
}
