<?php

namespace App\Http\Controllers;

use App\Models\Champion;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Slider;

class PostController extends Controller
{
    //
    public function store(Request $request) {
        $validated = validator($request->all(), [
            'title' => ['required', 'string', 'max:160'],
            'content' => ['required', 'string'],
        ])->validate();

    }

    public function show() {
        $sliders = Slider::all(['image']);
        $posts = Post::all(['id', 'title', 'image', 'description', 'created_at'])->sortByDesc('created_at')->take(3);
        $champions = Champion::all('image', 'name', 'id')->take(5);
        return view('index', compact('posts', 'sliders', 'champions'));
    }


}


