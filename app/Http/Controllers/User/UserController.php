<?php


namespace App\Http\Controllers\User;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index() {
        return view('user.person.index');
    }

    public function avatar ()
    {
        return view('user.person.avatar');
    }

    public function store ()
    {
        return 'Запрос создания поста';
    }

    public function upload (Request $request)
    {
        $validated = $request->validate([
            'image' => ['required', 'image:jpg, jpeg, png, gif'],
        ]);
        $request = Storage::put('public/uploads', $validated['image']);
        User::where('id', Auth::user()->id)
            ->update([
                'avatar' => $request
            ]);
        return redirect()->route('admin');

    }

    public function edit ($post)
    {
        view('user.posts.edit', compact('post'));
    }

    public function update ()
    {
        return 'Запрос изменения поста';
    }

    public function delete ()
    {
        return 'Запрос удаления поста';
    }
}
