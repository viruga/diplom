<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function index() {
        return view('login.index');
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:50'],
            'password' => ['required', 'string'],
        ]);

        if (Auth::attempt($validated)) {
            $request->session()->regenerate();
//            $data = $loginRequest->validated();
//            $data['admin'] = true;
            return redirect()->route('admin');
        }

        return back()->withErrors([
            'name' => 'Проверьте правильность ввода логина и пароля!',
        ])->onlyInput('name');

    }

    public function logout(Request $request)
    {
        Auth::logout();
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('home');
    }
}
