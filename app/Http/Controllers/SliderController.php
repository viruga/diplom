<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();

        return view('user.admin.slider.index', compact('sliders'));
    }

    public function create(Request $request)
    {
        return view('user.admin.slider.create');

    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => ['required', 'string', 'max:160'],
            'image' => ['required', 'image:jpg, jpeg, png, gif', 'dimensions:min_width=1280,min_height=440'],
        ]);


        if (isset($validated['image'])) {
            $requestIgm = Storage::put('public/uploads', $validated['image']);
        }

        $post = Slider::query()->create([
            'name' => $validated['title'],
            'image' => $requestIgm ?? null,
        ]);

        return redirect()->route('admin');
    }

    public function delete ($slide) {
        $post = Slider::query()->findOrFail($slide)->delete();
        return redirect()->route('admin');
    }
}
