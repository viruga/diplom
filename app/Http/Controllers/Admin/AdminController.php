<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PostController;
use App\Models\Champion;
use App\Models\Slider;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Post;


class AdminController extends Controller
{
    public function index() {
        $posts = Post::all(['id', 'title', 'image', 'created_at'])->sortByDesc('created_at')->take(10);
        $champions = Champion::all(['id', 'name'])->sortByDesc('created_at')->take(10);
        $sliders = Slider::all('id', 'name')->sortByDesc('created_at')->take(5);

        return view('user.admin.index', compact('posts', 'sliders', 'champions'));
    }

    public function create() {
        return view('user.admin.posts.create');
    }

    public function store(Request $request) {


        $validated = $request->validate([
            'title' => ['required', 'string', 'max:160'],
            'description' => ['required', 'string', 'max:250'],
            'content' => ['required', 'string', 'max:5000'],
            'image' => ['nullable', 'image:jpg, jpeg, png, gif'],
        ]);


        if (isset($validated['image'])) {
            $requestIgm = Storage::put('public/uploads', $validated['image']);
        }

        $post = Post::query()->create([
            'user_id' => Auth::id(),
            'title' => $validated['title'],
            'description' => $validated['description'],
            'content' => $validated['content'],
            'image' => $requestIgm ?? null,
        ]);

        return redirect()->route('admin.posts.create');
    }

    public function delete ($post) {
        $post = Post::query()->findOrFail($post)->delete();
        return redirect()->route('admin');
    }

    public function edit ($post) {
        $post = Post::query()->findOrFail($post);
        return view('user.admin.posts.edit', compact('post'));
    }

    public function update(Request $request) {


        $validated = $request->validate([
            'id' => ['required', 'int'],
            'title' => ['required', 'string', 'max:160'],
            'description' => ['required', 'string', 'max:250'],
            'content' => ['required', 'string', 'max:5000'],
            'image' => ['nullable', 'image:jpg, jpeg, png, gif'],
        ]);

        if (isset($validated['image'])) {
                $requestIgm = Storage::put('public/uploads', $validated['image']);

        }

        $post = Post::query()->where('id', '=', $validated['id'])->update([
            'user_id' => Auth::id(),
            'title' => $validated['title'],
            'description' => $validated['description'],
            'content' => $validated['content'],
            'image' => $requestIgm ?? null,
        ]);


        return redirect()->route('admin');
    }

}

