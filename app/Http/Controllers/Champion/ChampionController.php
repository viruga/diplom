<?php

namespace App\Http\Controllers\Champion;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Champion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Haracteristic;

class ChampionController extends Controller
{
    public function index()
    {
        $champions = Champion::all(['id', 'name'])->sortByDesc('created_at')->take(10);
        $cars = Car::all(['champion_id', 'model', 'brend', 'id'])->sortByDesc('created_at');

        return view('user.admin.champion.index', compact( 'champions','cars'));

    }


    public function show($champion)
    {
        $champion = Champion::query()->findOrFail($champion);
        $championsCol = Champion::all()->count();
        if ($championsCol<5) {
            $number = $championsCol;
        }
        else { $number = 5;}
        $champions = Champion::all()->random($number);

        $cars = Car::all();
        $haracteristics = Haracteristic::all();

        return view('user.admin.champion.show', compact('champion', 'champions', 'cars', 'haracteristics'));
    }

    public function edit($champion)
    {
        $champion = Champion::query()->findOrFail($champion);
        return view('user.admin.champion.edit', compact('champion'));
    }

    public function store(Request $request)
    {

        $validated = $request->validate([
            'name' => ['required', 'string', 'max:160'],
            'content' => ['required', 'string', 'max:5000'],
            'image' => ['nullable', 'image:jpg, jpeg, png, gif'],
        ]);

        if (isset($validated['image'])) {
            $requestIgm = Storage::put('public/uploads', $validated['image']);
        }

        $post = Champion::query()->create([
            'name' => $validated['name'],
            'content' => $validated['content'],
            'image' => $requestIgm ?? null,
        ]);

        return redirect()->route('admin');
    }

    public function update(Request $request) {

        $validated = $request->validate([
            'id' => ['required', 'int'],
            'name' => ['required', 'string', 'max:160'],
            'content' => ['required', 'string', 'max:5000'],
            'image' => ['nullable', 'image:jpg, jpeg, png, gif'],
        ]);

        if (isset($validated['image'])) {
            $requestIgm = Storage::put('public/uploads', $validated['image']);

        }

        $post = Champion::query()->where('id', '=', $validated['id'])->update([
            'name' => $validated['name'],
            'content' => $validated['content'],
            'image' => $requestIgm ?? null,
        ]);


        return redirect()->route('admin');
    }

    public function delete ($champion) {
        $champion = Champion::query()->findOrFail($champion)->delete();
        return redirect()->route('admin');
    }

    public function create()
    {
        return view('user.admin.champion.create');

    }
}
