<?php

namespace App\Http\Controllers\Car;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Champion;
use App\Models\Haracteristic;
use Illuminate\Http\Request;


class CarController extends Controller
{
    public function show($id)
    {
        $car = Car::query()->findOrFail($id);
        $haracteristics = Haracteristic::all()->where('car_id', '=', $car->id);
        return view('user.admin.car.index', compact( 'car','haracteristics'));

    }

    public function create()
    {
        $champions = Champion::all(['id', 'name'])->sortByDesc('created_at');
        return view('user.admin.car.create', compact( 'champions'));

    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'champion' => ['required', 'int'],
            'brend' => ['required', 'string', 'max:250'],
            'model' => ['required', 'string', 'max:250'],
        ]);

        $car = Car::query()->create([
            'champion_id' => $validated['champion'],
            'brend' => $validated['brend'],
            'model' => $validated['model'],
        ]);

        return redirect()->route('champion');
    }

    public function delete ($car) {
        $cars = Car::query()->findOrFail($car)->delete();
        return redirect()->route('champion');
    }


}
