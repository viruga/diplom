<?php

namespace App\Http\Controllers\Car;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Haracteristic;
use Illuminate\Http\Request;

class HaracteristicController extends Controller
{
    public function create()
    {
        $cars = Car::all(['id', 'brend', 'model'])->sortBy('brend');
        return view('user.admin.haracteristic.create', compact( 'cars'));


    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'car' => ['required', 'int'],
            'name' => ['required', 'string', 'max:250'],
            'text' => ['required', 'string', 'max:250'],
        ]);

        $car = Haracteristic::query()->create([
            'car_id' => $validated['car'],
            'name' => $validated['name'],
            'text' => $validated['text'],
        ]);

        return redirect()->route('champion');
    }

    public function delete ($id) {
        $champion = Haracteristic::query()->findOrFail($id)->delete();
        return redirect()->route('champion');
    }
}
