<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AdminMiddleware
{

    public function handle(Request $request, Closure $next)
    {
          $user = $request->user();

        if ($user->admin === 0) {
            return redirect()->route('person');
        }
//        abort_unless($user->admin, 403);
        return $next($request);
    }
}
