<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Haracteristic extends Model
{
    use HasFactory;
    protected $fillable = [
        'car_id',
        'name', 'text',
    ];

    public $timestamps = false;
}
